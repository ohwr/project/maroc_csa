'''
Module for running every single test available of the PyChips source code.

NOTE:  The script "startDummyHardwareUdp.py" must be already
       running for all the tests to complete properly 

Created on May 21, 2010
@author: Robert Frazier
'''

#**************  IMPORTANT NOTE  ********************
# The script "startDummyHardwareUdp.py" must be already 
# running for all the tests to complete properly.
#**************  IMPORTANT NOTE  ********************

import unittest
from Test_CommonTools import *
from Test_IPbusHeader import *
from Test_TransactionElement import *
from Test_AddressTableItem import *
from Test_AddressTable import *
from Test_SerDes import *
from Test_ChipsBus import *

if __name__ == '__main__':
    """Runs all the tests we have"""
    
    unittest.main()
    
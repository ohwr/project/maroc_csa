
# Xilinx ISE setup fragment for gig_eth_pcs_pma_v11_5

files = [
    "../../ethernet/coregen/tri_mode_eth_mac_v5_4.xco",
    "../../ethernet/coregen/gig_eth_pcs_pma_v11_4.xco",
    "../hdl/eth_s6_1000basex.vhd",
    "../hdl/emac_hostbus_decl.vhd",
    "../../../../ipcore_dir/gig_eth_pcs_pma_v11_4/example_design/gig_eth_pcs_pma_v11_4_block.vhd" ,
    "../../../../ipcore_dir/gig_eth_pcs_pma_v11_4/example_design/transceiver/gig_eth_pcs_pma_v11_4_transceiver_A.vhd",
    "../../../../ipcore_dir/gig_eth_pcs_pma_v11_4/example_design/transceiver/gig_eth_pcs_pma_v11_4_s6_gtpwizard.vhd",
    "../../../../ipcore_dir/gig_eth_pcs_pma_v11_4/example_design/transceiver/gig_eth_pcs_pma_v11_4_s6_gtpwizard_tile.vhd",
    "../sim/eth_mac_sim.vhd"
    ]


#    "../../ethernet/coregen/coregen/tri_mode_eth_mac_v5_4.xco",
#    "../../ethernet/coregen/coregen/gig_eth_pcs_pma_v11_4.xco",

